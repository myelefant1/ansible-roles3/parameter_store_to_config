Parameter Store to config
===

Install and configure systemd units that generate configuration files from templates and data downloaded from AWS Parameter Store.

Rendering of templates is performed using [repasto](https://gitlab.com/myelefant1/repasto).

Tested on debian 10.

## Role parameters

| name                                    | type   | optional  | default value    | description             |
| ----------------------------------------|--------|-----------|------------------|-------------------------|
| parameter_store_to_config_owner         | string | yes       | root             | user owning output file |
| parameter_store_to_config_group         | string | yes       | root | user group owning output file       |
| parameter_store_to_config_aws_region    | string | no        | | AWS region used to query Parameter Store |
| parameter_store_to_config_output        | string | no        |                  | output full path        |
| parameter_store_to_config_service_state | string | no        |                  | [systemd unit service state](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_module.html#parameter-state)   |
| parameter_store_to_config_mode          | string | yes       | 0644             | output file mode        |
| parameter_store_to_config               | dict   | no        |                  | see below               |


`parameter_store_to_config` is a dict of `name`/`settings` representing each part of configuration to include in finale output. `name` is a string identifying part of configuration and settings is a dict of key/value:
|key|description|
|---|-----------|
|parameter_path | Parameter store path to request |
|templates_dir | Directory where repasto would look up for templates |
|template_dest| The value represents the remote location where template file is to be copied |
|template| The name of the template, both used to copy template to remote and to set which template is to be used by `repasto` for rendering |
|render_safe_mode | Pass 'safe-mode' option to repasto command (*) |

(*): this is sometimes required depending on paths used in Parameter Store and templates used to render those. Check out [repasto](https://gitlab.com/myelefant1/repasto) for details.

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/parameter_store_to_config.git
  scm: git
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: parameter_store_to_config
      # shared variables
      templates_dir: "/usr/local/src/parameter_store_to_config"
      template_dest: "/usr/local/src/parameter_store_to_config/project"
      template: "project/full_ini.template"
      user: "root"
      mode: "0644"
      # actual settings
      parameter_store_to_config_owner: "{{ user }}"
      parameter_store_to_config_group: "{{ user }}"
      parameter_store_to_config_aws_region: "eu-west-1"
      parameter_store_to_config_output: "/usr/local/etc/full.ini"
      parameter_store_to_config_service_state: "started"
      parameter_store_to_config_mode: "{{ mode }}"
      parameter_store_to_config:
        config:
          parameter_path: "/connectors/config/"
          templates_dir: "{{ templates_dir }}"
          template_dest: "{{ template_dest }}"
          template: "{{ template }}"
        common:
          parameter_path: "/connectors/common/"
          templates_dir: "{{ templates_dir }}"
          template_dest: "{{ template_dest }}"
          template: "{{ template }}"
```
